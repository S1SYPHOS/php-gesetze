<?php

namespace S1SYPHOS\Gesetze\Providers\Provider;

use S1SYPHOS\Gesetze\Providers\Provider;


/**
 * Class Buzer
 *
 * Provider for 'buzer.de'
 */
class Buzer extends Provider
{
    /**
     * Properties
     */

    /**
     * Individual identifier
     *
     * @var string
     */
    protected $identifier = 'buzer';


    /**
     * Base URL
     *
     * @var string
     */
    protected $url = 'https://buzer.de';
}
